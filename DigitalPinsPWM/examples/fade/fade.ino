#include <DigitalPinsPWM.h>

#define CYCLE_COUNT 10
#define DELAY_SMALL 1
#define DELAY_LARGE 1000

int counter = 0;

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);

  DigitalPins.init();
}

void loop()
{
  for (int i = 0 ; i != 256 ; ++i)
  {
    DigitalPins.analogWrite(LED_BUILTIN, i);
    delay(DELAY_SMALL);
  }
  delay(DELAY_LARGE);
  for (int i = 255 ; i >= 0 ; --i)
  {
    DigitalPins.analogWrite(LED_BUILTIN, i);
    delay(DELAY_SMALL);
  }
  delay(DELAY_LARGE);
  if (++counter == CYCLE_COUNT)
  {
    DigitalPins.uninit();
  }
}

