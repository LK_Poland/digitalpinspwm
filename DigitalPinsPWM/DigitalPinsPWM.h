#ifndef DIGITAL_PINS_PWM
#define DIGITAL_PINS_PWM

class DigitalPinsPWM
{
    public:
        void init();
        void uninit();

        void analogWrite(int pin_index, int value);

    private:
        static void digitalWriteLOW(int pin_index);
        static void digitalWriteHIGH(int pin_index);

        static void doPWM();

    private:
        static const int maxPWM = 256;

        struct Pin
        {
                Pin(int i) : idx(i), ctr(0), pwm(0), sta(-1)
                {
                }
                int idx;
                int ctr;
                int pwm;
                int sta;
        };

        static volatile Pin pin2, pin4, pin7, pin8, pin12, pin13;
        static Pin* pins[6] = {&pin2, &pin4, &pin7, &pin8, &pin12, &pin13};

        const int pwm_pins[6] = {3, 5, 6, 9, 10, 11};
        const int pwm_pins_count = sizeof(pwm_pins) / sizeof(int);

        static const int non_pwm_pins[6] = {2, 4, 7, 8, 12, 13};
        static const int non_pwm_pins_count = sizeof(non_pwm_pins) / sizeof(int);
};

extern DigitalPinsPWM DigitalPins;

#endif
