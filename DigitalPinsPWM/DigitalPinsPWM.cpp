#include <Arduino.h>
#include <DigitalPinsPWM.h>
#include <TimerOne.h>

DigitalPinsPWM DigitalPins;

void DigitalPinsPWM::init()
{
    Timer1.initialize(8);
    Timer1.attachInterrupt(doPWM);
}

void DigitalPinsPWM::uninit()
{
    Timer1.stop();
    Timer1.detachInterrupt();
}

void DigitalPinsPWM::analogWrite(int pin_index, int value)
{
    if (pin_index < 0 || pin_index > 13)
    {
        return;
    }
    if (value < 0)
    {
        value = 0;
    }
    else if (value > 255)
    {
        value = 255;
    }
    for (int i = 0 ; i != pwm_pins_count ; ++i)
    {
        if (pin_index == pwm_pins[i])
        {
            ::analogWrite(pin_index, value);
            return;
        }
    }
    Pin *pin = NULL;
    switch (pin_index)
    {
        case 2:
            pin = &pin2;
        break;
        case 4:
            pin = &pin4;
        break;
        case 7:
            pin = &pin7;
        break;
        case 8:
            pin = &pin8;
        break;
        case 12:
            pin = &pin12;
        break;
        case 13:
            pin = &pin13;
        break;
    }
    noInterrupts();
    pin->ctr = 0;
    pin->pwm = value;
    pin->sta = pin->sta < 0 ? LOW : pin->sta;
    interrupts();
}

static void DigitalPinsPWM::digitalWriteLOW(int pin_index)
{
    // B is digital pins 8 - 13.
    // D is digital pins 0 - 7.
    byte b = 1;
    if (pin_index < 8)
    {
        b <<= pin_index;
        b ^= 0xFF;
        PORTD &= b;
    }
    else// if (pin_index <= 13)
    {
        b <<= (pin_index - 8);
        b ^= 0xFF;
        PORTB &= b;
    }
}

static void DigitalPinsPWM::digitalWriteHIGH(int pin_index)
{
    // B is digital pins 8 - 13.
    // D is digital pins 0 - 7.
    byte b = 1;
    if (pin_index < 8)
    {
        b <<= pin_index;
        PORTD |= b;
    }
    else// if (pin_index <= 13)
    {
        b <<= (pin_index - 8);
        PORTB |= b;
    }
}

static void DigitalPinsPWM::doPWM()
{
    for (int i = 0 ; i != non_pwm_pins_count ; ++i)
    {
        Pin &pin = *pins[i];
        if (pin.sta < 0)
        {
            continue;
        }
        if (pin.ctr < pin.pwm)
        {
            if (pin.sta != HIGH)
            {
                digitalWriteHIGH(pin.idx);
                pin.sta = HIGH;
            }
        }
        else
        {
            if (pin.sta != LOW)
            {
                digitalWriteLOW(pin.idx);
                pin.sta = LOW;
            }
        }
        if (++pin.ctr >= maxPWM)
        {
            pin.ctr = 0;
        }
    }
}

volatile DigitalPinsPWM::Pin DigitalPinsPWM::pin2 = Pin(2);
volatile DigitalPinsPWM::Pin DigitalPinsPWM::pin4 = Pin(4);
volatile DigitalPinsPWM::Pin DigitalPinsPWM::pin7 = Pin(7);
volatile DigitalPinsPWM::Pin DigitalPinsPWM::pin8 = Pin(8);
volatile DigitalPinsPWM::Pin DigitalPinsPWM::pin12 = Pin(12);
volatile DigitalPinsPWM::Pin DigitalPinsPWM::pin13 = Pin(13);

DigitalPinsPWM::Pin* DigitalPinsPWM::pins[6];
