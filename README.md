# DigitalPinsPWM

Arduino library providing unified interface for using PWM on all digital pins of Arduino UNO (pins from 0 to 13). PWM on pins not supporting it in hardware is software emulated.

# Installation

1. Install TimerOne library if not present.
2. Copy DigitalPinsPWM folder to Arduino\libraries directory.

# Example

From Arduino IDE open: File->Examples->DigitalPinsPWM->fade.